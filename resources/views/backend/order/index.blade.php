@extends("backend.layout.layout")
@section("title","Order List")
@section("content")

<div class="fl-page-section">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"><b>Order List</b></h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive">

                    <div class="col-md-12">
                        <form method="get" action="{{route("backend.order.view")}}">
                            @csrf
                            <div class="row">
                                @if(Auth::user()->role_id != 10)
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="name">Vendor Name</label>
                                        <div class="md-form mt-0">
                                            <select name="vendor_id" class="form-control single-select2" id="vendor_id" style="width: 100%;" required>
                                                <option selected disabled>Select One</option>
                                                @foreach($users as $uk => $uv)
                                                    <option value="{!! $uv->id !!}" @if($request->vendor_id==$uv->id) selected @endif>{!! $uv->name !!}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="name">Order no.</label>
                                        <div class="md-form mt-0">
                                            <input type="text" class="form-control" id="order_no" name="order_no" placeholder="Enter Order no." value="{{$request->order_no}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="name">Order Status</label>
                                        <div class="md-form mt-0">
                                            <select name="order_status" class="form-control single-select2" id="order_status" style="width: 100%;" required>
                                                <option selected disabled>Select One</option>
                                                <option value="1" @if($request->order_status==1) selected @endif>Pending</option>
                                                <option value="2" @if($request->order_status==2) selected @endif>Processing</option>
                                                <option value="3" @if($request->order_status==3) selected @endif>Complete</option>
                                                <option value="4" @if($request->order_status==4) selected @endif>On Holod</option>
                                                <option value="5" @if($request->order_status==5) selected @endif>Cancel</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="start_date">Start Date</label>
                                        <input type="text" class="form-control datepicker" id="start_date" name="start_date" value="{{$request->start_date}}" autocomplete="off" readonly="">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="end_date">End Date</label>
                                        <input type="text" class="form-control " id="end_date" name="end_date" value="{{$request->end_date}}" readonly="">
                                    </div>
                                </div>
                                <div class="col-md-3" style="padding-top: 26px;">
                                    <button type="submit" class="btn btn btn-dark float-right text-white">
                                        <i class="fas fa-search"></i>
                                        Search
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>SL</th>
                                @if(Auth::user()->role_id != 10)
                                    Vendor Name
                                @endif
                                <th>Order No</th>
                                <th>Total Qty</th>
                                <th>Total Cost</th>
                                <th>Payment Method</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                use App\CustomClass\OwnLibrary;
                                $i = OwnLibrary::paginationSerial($orders);
                            @endphp
                            @forelse($orders as $order)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{ $order->oid }}</td>
                                <td class="text-right">{{ $order->quantity }}</td>
                                <td class="text-right">{{ $order->order_total }}</td>
                                <td class="text-right">{{ $order->payment_method }}</td>
                                <td class="text-center">
                                    @if($order->order_status == 1)
                                        <button class="btn btn-sm btn-primary">Pending</button>
                                    @elseif($order->order_status == 2)
                                        <button class="btn btn-sm btn-info">Processing</button>
                                    @elseif($order->order_status == 3)
                                        <button class="btn btn-sm btn-success">Complete</button>
                                    @elseif($order->order_status == 4)
                                        <button class="btn btn-sm btn-warning">On Hold</button>
                                    @elseif($order->order_status == 5)
                                        <button class="btn btn-sm btn-danger">Cancel</button>
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-sm btn-success text-white" href="{{route('backend.order.details',$order->oid)}}" title="Details" target="_blank">
                                            <i class="fas fa-eye"></i>
                                    </a>
                                </td>
                                
                            </tr>
                            @empty
                            <tr>
                                <td colspan="8" class="text-center">Nothing Found</td>
                            </tr>
                            @endforelse

                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="card-footer clearfix text-right">
                    {{$orders->links("backend.include.pagination")}}
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('js')
<script>
    $(document).ready(function(){
        var expired_date_start = "";
        $('.datepicker').datetimepicker({
                format: 'yyyy-mm-dd',
                weekStart: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                minView: 2, 
                pickTime: false, 
                //showMeridian: 1,

            }).on('changeDate', function(ev){
                var orgDate = new Date(ev.date);
                var getDate = orgDate.getFullYear()+'-'+(orgDate.getMonth()+1)+'-'+orgDate.getDate()+" "+orgDate.getHours()+":"+orgDate.getMinutes();
                expired_date_start = getDate;

                $("expired_date").val("");
                $('#end_date').datetimepicker({
                    format: 'yyyy-mm-dd',
                    weekStart: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    forceParse: 0,
                    minView: 2, 
                    pickTime: false, 
                    startDate: expired_date_start
                });
            });
    });
</script>
@endsection

