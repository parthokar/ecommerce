@extends("backend.layout.layout")
@section("title","Order Details")
<style type="text/css">
    table.billing.table th, table.billing.table td {
        padding-top: 0.2rem !important;
        padding-bottom: .2rem !important;
    }
    .billing td, .billing th {
        padding: .75rem .75rem .0rem .75rem !important;
    }
</style>
@section("content")

<div class="fl-page-section">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"><b>Order Details</b></h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive">

                    <table class="table table-borderless billing">
                        <tr>
                            <td>Order Date:</td>
                            <td>{!! $orders->created_at->format('d-M-Y') !!}</td>
                            <td>Billing Address:</td>
                            <td>{!! $shipping->address.', '.$shipping->city->name.', '.$shipping->state->name.', '.$shipping->country->name !!}</td>
                        </tr>
                        <tr>
                            <td>Order No:</td>
                            <td>{!! str_pad($orders->id,5,'0',STR_PAD_LEFT) !!}</td>
                            <td>Billing Email:</td>
                            <td>{!! $shipping->email !!}</td>
                        </tr>
                        <tr>
                            <td>Customer Name:</td>
                            <td>{!! $shipping->customer->name !!}</td>
                            <td>Billing Phone:</td>
                            <td>{!! $shipping->contact_no !!}</td>
                        </tr>
                        <tr>
                            <td>Shipping Method:</td>
                            <td>{!! $orders->shipping_method_name->method_name !!}</td>
                            <td>Payment Method:</td>
                            <td>{!! $orders->payment_gateway->name !!}</td>
                        </tr>
                    </table><br>

                    @if($order_details)
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>Cost</th>
                                <th>Total Cost</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                                $i = 1; 
                                $total_sub = 0;
                                $discount  = 0;
                            ?>
                            @forelse($order_details as $od)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td><img src="{!! asset($od->product->feature_image) !!}" alt="" style="height: 50px;width: 50px;"> &nbsp;&nbsp; {{ $od->product->name }}</td>
                                    <td class="text-right">{{ $od->quantity }}</td>
                                    <td class="text-right">{{ $od->price }}</td>
                                    <td class="text-right">{{ $od->price*$od->quantity }}</td>
                                </tr>
                                @php
                                    $total_sub  += $od->price*$od->quantity;
                                    $discount += $od->discount;
                                @endphp
                            @empty
                                <tr>
                                    <td colspan="5" class="text-center">Nothing Found</td>
                                </tr>
                            @endforelse

                            @if($orders->order_shipping OR $orders->order_tax OR $discount)
                                <tr>
                                    <td colspan="4" class="text-right"><b>Sub Total</b></td>
                                    <td class="text-right"><b>{{ $total_sub }}</b></td>
                                </tr>

                                @if($orders->order_tax)
                                    <tr>
                                        <td colspan="4" class="text-right">VAT</td>
                                        <td class="text-right">{{ $orders->order_tax }}</td>
                                    </tr>
                                    @php
                                        $total_sub += $orders->order_tax;
                                    @endphp
                                @endif
                                @if($orders->order_shipping)
                                    <tr>
                                        <td colspan="4" class="text-right">Delivery Charge</td>
                                        <td class="text-right">{{ $orders->order_shipping }}</td>
                                    </tr>
                                    @php
                                        $total_sub += $orders->order_shipping;
                                    @endphp
                                @endif
                                @if($orders->order_shipping OR $orders->order_tax)
                                    <tr>
                                        <td colspan="4" class="text-right"><b>Total</b></td>
                                        <td class="text-right"><b>{{ $total_sub }}</b></td>
                                    </tr>
                                @endif
                                @if($discount)
                                    <tr>
                                        <td colspan="4" class="text-right">(Discoun)</td>
                                        <td class="text-right">{{ $discount }}</td>
                                    </tr>
                                    @php
                                        $total_sub -= $discount;
                                    @endphp
                                    <tr>
                                        @if($orders->order_shipping OR $orders->order_tax)
                                            <td colspan="4" class="text-right"><b>Grand total</b></td>
                                        @else
                                            <td colspan="4" class="text-right"><b>Total</b></td>
                                        @endif
                                        <td class="text-right"><b>{{ $total_sub }}</b></td>
                                    </tr>
                                @endif

                            @else
                                <tr>
                                    <td colspan="4" class="text-right"><b>Total</b></td>
                                    <td class="text-right"><b>{{ $total_sub }}</b></td>
                                </tr>
                            @endif

                            </tbody>
                        </table>
                    @endif

                    <div class="row" style="padding-top: 15px;">
                        <div class="col-md-4"></div>
                        <div class="col-md-1">Status:</div>
                        <div class="col-md-3">
                            <div class="form-group select2-parent">
                                <select name="order_status" class="form-control single-select2" id="order_status" style="width: 100%;" required>
                                    <option selected disabled>Select One</option>
                                    <option value="1" @if($orders->order_status==1) selected @endif>Pending</option>
                                    <option value="2" @if($orders->order_status==2) selected @endif>Processing</option>
                                    <option value="3" @if($orders->order_status==3) selected @endif>Complete</option>
                                    <option value="4" @if($orders->order_status==4) selected @endif>On Holod</option>
                                    <option value="5" @if($orders->order_status==5) selected @endif>Cancel</option>
                                </select>
                                <div class="spinner">&nbsp;</div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <a href="{{route('backend.order.details',$orders->id.'/print')}}" id="print" class="btn btn-sm btn-primary cancel" target="_blank">Print</a>
                            <a href="{!! url('orders') !!}" class="btn btn-sm btn-warning cancel">Back</a>
                        </div>
                    </div>
                    
                </div>
                <!-- /.card-body -->
                
            </div>
        </div>
    </div>

</div>

@endsection

@section('js')
<script>
    $(document).ready(function(){
        
        $('#order_status').on('change', function (e) {
            var me = $(this);
            var spinner = $(this).closest('div').find('.spinner');
            spinner.show();

            var value       = $('#order_status option:selected').text();
            var order_value = $('#order_status').val();
            var order_id    = "{!! $orders->id !!}";
            var order_status_default_select = "{!! $orders->status !!}";;
            
            e.preventDefault();
            swal.fire({
                title: 'Please Confirm',
                text: "Order Is "+value+" ??",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: ' No!',
                confirmButtonText: 'Yes!'
            }).then((result) => {
                if (result.value) {
                $.ajax({
                    url: "{{ route('backend.orders-status-change')}}",
                    type: "post",
                    data: {"order_value": order_value,"order_id":order_id, _token: '{{csrf_token()}}'},
                    success:function(data) {
                        var message_txt = null;
                        var msg_type = null;
                        if(data=='success'){
                            var message_txt = "Order Is "+value;
                            msg_type = 'success';
                        }
                        else if(data=="faild"){
                            var message_txt = "Something went wrong. Please try again later.";
                            msg_type = 'error';
                        }
                        
                        swal.fire({
                            text: message_txt,
                            type: msg_type
                        });
                        spinner.hide();
                    }
                });

            }else{
                $('#order_status').val(order_status_default_select).select2();
                spinner.hide();
            }
        });

             
        });

    });
</script>
@endsection

