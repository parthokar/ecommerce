<li class="fl-menu-item">
    <a href="{{route('shop-setting.index')}}" class="fl-menu-link @if($routeName == 'shop-setting.index') active @endif">
        <i class="menu-item-icon icon ion-ios-home-outline tx-24"></i>
        <span class="menu-item-label">Shop Setting</span>
    </a><!-- fl-menu-link -->
</li>

<li class="fl-menu-item">
    <a href="#" class="fl-menu-link with-sub @if($routeName == 'product.index') active @endif">
        <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
        <span class="menu-item-label">Product</span>
    </a><!-- fl-menu-link -->
    <ul class="fl-menu-sub">
        <li class="sub-item">
            <a href="{{route('products.create')}}" class="sub-link">Create Product</a>
        </li>
        <li class="sub-item">
            <a href="{{route('products.index')}}" class="sub-link">Product List</a>
        </li>
    </ul>
</li>
