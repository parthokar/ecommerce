<li class="fl-menu-item">
    <a href="#" class="fl-menu-link with-sub @if($routeName == 'payment-gateway.index') active @endif">
        <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
        <span class="menu-item-label">Payment Setting</span>
    </a><!-- fl-menu-link -->
    <ul class="fl-menu-sub">
        <li class="sub-item">
            <a href="{{route('payment-gateway.index')}}" class="sub-link">Payment Gateway</a>
        </li>
        <li class="sub-item">
            <a href="{{route('currencys.index')}}" class="sub-link">Currency</a>
        </li>
    </ul>
</li>

<li class="fl-menu-item">
    <a href="#" class="fl-menu-link with-sub @if($routeName == 'email-configuration.index') active @endif">
        <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
        <span class="menu-item-label">Email</span>
    </a><!-- fl-menu-link -->
    <ul class="fl-menu-sub">
        <li class="sub-item">
            <a href="{{route('email-configuration.index')}}" class="sub-link">Email Configuration</a>
        </li>
    </ul>
</li>

<li class="fl-menu-item">
    <a href="#" class="fl-menu-link with-sub @if($routeName == 'product.index') active @endif">
        <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
        <span class="menu-item-label">Product</span>
    </a><!-- fl-menu-link -->
    <ul class="fl-menu-sub">
        <li class="sub-item">
            <a href="{{route('products.create')}}" class="sub-link">Create Product</a>
        </li>
        <li class="sub-item">
            <a href="{{route('products.index')}}" class="sub-link">Product List</a>
        </li>
    </ul>
</li>

<li class="fl-menu-item">
    <a href="#" class="fl-menu-link with-sub @if($routeName == 'vendor-subscription-plan.index') active @endif">
        <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
        <span class="menu-item-label">Vendor</span>
    </a><!-- fl-menu-link -->
    <ul class="fl-menu-sub">
        <li class="sub-item">
            <a href="{{route('vendor-subscription-plan.index')}}" class="sub-link">Vendor Subscription Plan</a>
        </li>
        {{--<li class="sub-item">--}}
            {{--<a href="{{route('products.index')}}" class="sub-link">Product List</a>--}}
        {{--</li>--}}
    </ul>
</li>
