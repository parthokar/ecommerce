<li class="fl-menu-item">
    <a href="#" class="fl-menu-link with-sub">
        <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
        <span class="menu-item-label">Lemon</span>
    </a><!-- fl-menu-link -->
    <ul class="fl-menu-sub">
        <li class="sub-item"><a href="{{route('discount.index')}}" class="sub-link">Discount</a></li>
        <li class="sub-item"><a href="{{route('shipping-option.index')}}" class="sub-link">Shipping Option</a></li>
        {{--<li class="sub-item"><a href="{{route('shop-setting.index')}}" class="sub-link">Shop Setting</a></li>--}}
        <li class="sub-item"><a href="{{route('vendor-page.index')}}" class="sub-link">Vendor Pages</a></li>
        <li class="sub-item"><a href="{{route('translation-language.index')}}" class="sub-link">Translation Languages</a></li>
        <li class="sub-item"><a href="{{route('countries.index')}}" class="sub-link">Countries</a></li>
        <li class="sub-item"><a href="{{route('state.index')}}" class="sub-link">States</a></li>
        <li class="sub-item"><a href="{{route('city.index')}}" class="sub-link">Cities</a></li>
        <li class="sub-item"><a href="{{route('store-review.index')}}" class="sub-link">Store Review</a></li>
        <li class="sub-item"><a href="{{route('backend.order.view')}}" class="sub-link">Orders</a></li>
    </ul>
</li>
