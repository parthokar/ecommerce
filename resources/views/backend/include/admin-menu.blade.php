<li class="fl-menu-item">
    <a href="index.html" class="fl-menu-link active">
        <i class="menu-item-icon icon ion-ios-home-outline tx-24"></i>
        <span class="menu-item-label">Dashboard</span>
    </a><!-- fl-menu-link -->
</li><!-- fl-menu-item -->
<li class="fl-menu-item">
    <a href="" class="fl-menu-link">
        <i class="menu-item-icon icon ion-ios-email-outline tx-24"></i>
        <span class="menu-item-label">Mailbox</span>
    </a><!-- fl-menu-link -->
</li><!-- fl-menu-item -->
<li class="fl-menu-item">
    <a href="#" class="fl-menu-link with-sub">
        <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
        <span class="menu-item-label">Cards &amp; Widgets</span>
    </a><!-- fl-menu-link -->
    <ul class="fl-menu-sub">
        <li class="sub-item"><a href="#" class="sub-link">Dashboard</a></li>
        <li class="sub-item"><a href="#" class="sub-link">Blog &amp; Social Media</a></li>
        <li class="sub-item"><a href="#" class="sub-link">Shop &amp; Listing</a></li>
    </ul>
</li>
<li class="fl-menu-item">
    <a href="#" class="fl-menu-link with-sub">
        <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
        <span class="menu-item-label">Extra</span>
    </a><!-- fl-menu-link -->
    <ul class="fl-menu-sub">
        <li class="sub-item"><a href="{{route('extra.form1')}}" class="sub-link">Table</a></li>
        <li class="sub-item"><a href="{{route('extra.form2')}}" class="sub-link">Form</a></li>
    </ul>
</li>





{{-- @if(!empty($aclList[6][1])) --}}
<li class="fl-menu-item">
    <a href="#" class="fl-menu-link with-sub">
        <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
        <span class="menu-item-label">User Management</span>
    </a><!-- fl-menu-link -->
    <ul class="fl-menu-sub">
        <li class="sub-item"><a href="{{route('user.index')}}" class="sub-link">List</a></li>
        <li class="sub-item"><a href="{{route('user.create')}}" class="sub-link">Create</a></li>
    </ul>
</li>
{{-- @endif --}}

{{-- @if(!empty($aclList[1][1])) --}}
<li class="fl-menu-item">
    <a href="#" class="fl-menu-link with-sub">
        <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
        <span class="menu-item-label">Role Management</span>
    </a><!-- fl-menu-link -->
    <ul class="fl-menu-sub">
        <li class="sub-item"><a href="{{route('role.index')}}" class="sub-link">List</a></li>
        <li class="sub-item"><a href="{{route('role.create')}}" class="sub-link">Create</a></li>
    </ul>
</li>
{{-- @endif --}}

{{-- @if(!empty($aclList[2][1])) --}}
<li class="fl-menu-item">
    <a href="#" class="fl-menu-link with-sub">
        <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
        <span class="menu-item-label">Role Access Control</span>
    </a><!-- fl-menu-link -->
    <ul class="fl-menu-sub">
        <li class="sub-item"><a href="{{route('role.access')}}" class="sub-link">Create</a></li>
    </ul>
</li>
{{-- @endif --}}

{{-- @if(!empty($aclList[3][1])) --}}
<li class="fl-menu-item">
    <a href="#" class="fl-menu-link with-sub">
        <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
        <span class="menu-item-label">User Access Control</span>
    </a><!-- fl-menu-link -->
    <ul class="fl-menu-sub">
        <li class="sub-item"><a href="{{route('user.access')}}" class="sub-link">Create</a></li>
    </ul>
</li>
{{-- @endif --}}

{{-- @if(!empty($aclList[4][1])) --}}
<li class="fl-menu-item">
    <a href="#" class="fl-menu-link with-sub">
        <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
        <span class="menu-item-label">Module Management</span>
    </a><!-- fl-menu-link -->
    <ul class="fl-menu-sub">
        <li class="sub-item"><a href="{{route('module.index')}}" class="sub-link">List</a></li>
        <li class="sub-item"><a href="{{route('module.create')}}" class="sub-link">Create</a></li>
    </ul>
</li>
{{-- @endif --}}

{{-- @if(!empty($aclList[5][1])) --}}
<li class="fl-menu-item">
    <a href="#" class="fl-menu-link with-sub">
        <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
        <span class="menu-item-label">Activity Management</span>
    </a><!-- fl-menu-link -->
    <ul class="fl-menu-sub">
        <li class="sub-item"><a href="{{route('activity.index')}}" class="sub-link">List</a></li>
        <li class="sub-item"><a href="{{route('activity.create')}}" class="sub-link">Create</a></li>
    </ul>
</li>
{{-- @endif --}}

@include("backend.include.farhan")
@include("backend.include.lemon")
@include("backend.include.partho")
@include("backend.include.miraj")
