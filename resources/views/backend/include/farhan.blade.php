<li class="fl-menu-item">
    <a href="#"
        class="fl-menu-link with-sub @if($routeName == 'category.index'||$routeName == 'brand.index')active @endif">
        <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
        <span class="menu-item-label">Catalog Management</span>
    </a><!-- fl-menu-link -->
    <ul class="fl-menu-sub">
        <li class="sub-item"><a href="{{route('category.index')}}" class="sub-link">Categories</a></li>
        <li class="sub-item"><a href="{{route('brand.index')}}" class="sub-link">Brand</a></li>
    </ul>
</li>

<li class="fl-menu-item">
    <a href="#"
        class="fl-menu-link with-sub @if($routeName == 'pages.index'||$routeName == 'navigation.index'||$routeName == 'home-page-banner.index'||$routeName == 'social.link.edit' ||$routeName == 'advertisement.edit'||$routeName == 'advertisement.second.edit') active @endif">
        <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
        <span class="menu-item-label">Content Management</span>
    </a><!-- fl-menu-link -->
    <ul class="fl-menu-sub">
        <li class="sub-item"><a href="{{route('pages.index')}}" class="sub-link">Page</a></li>
        <li class="sub-item"><a href="{{route('navigation.index')}}" class="sub-link">Navigation</a></li>
        <li class="sub-item"><a href="{{route('home-page-banner.index')}}" class="sub-link">Home Page Banner</a></li>
        <li class="sub-item"><a href="{{route('social.link.edit')}}" class="sub-link">Social Link</a></li>
        <li class="sub-item"><a href="{{route('site.setting.edit')}}" class="sub-link">Site Settings</a></li>
        <li class="sub-item"><a href="{{route('advertisement.edit',"banner_portrait_top")}}"
                class="sub-link">Advertisemet 1</a></li>
        <li class="sub-item"><a href="{{route('advertisement.edit',"banner_portrait_bottom")}}"
                class="sub-link">Advertisemet 2</a></li>
        <li class="sub-item"><a href="{{route('advertisement.edit',"promotion_offer_first")}}"
                class="sub-link">Promotion First</a></li>
        <li class="sub-item"><a href="{{route('advertisement.edit',"promotion_offer_second")}}"
                class="sub-link">Promotion
                Second</a></li>
        <li class="sub-item"><a href="{{route('advertisement.edit',"promotion_offer_third")}}"
                class="sub-link">Promotion
                Third</a></li>
    </ul>
</li>

<li class="fl-menu-item">
    <a href="#" class="fl-menu-link with-sub @if($routeName == 'social-login-access.index') active @endif">
        <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
        <span class="menu-item-label">Social Login</span>
    </a><!-- fl-menu-link -->
    <ul class="fl-menu-sub">
        <li class="sub-item"><a href="{{route('social-login-access.index')}}" class="sub-link">Social Login Access</a>
        </li>
    </ul>
</li>

<li class="fl-menu-item">
    <a href="#" class="fl-menu-link with-sub @if($routeName == 'home.setup.edit') active @endif">
        <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
        <span class="menu-item-label">Home Setup</span>
    </a><!-- fl-menu-link -->
    <ul class="fl-menu-sub">
        <li class="sub-item"><a href="{{route('home.setup.edit')}}" class="sub-link">Seleted Category</a>
        </li>
    </ul>
</li>
