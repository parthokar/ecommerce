@extends('frontend.dashboard.layout.layout')
@section('user-d-title','User Details')
@section('user-d-content')
    <div class="customer_d_tab_right">
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane  active" id="d_customer_info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="d_customer_row_title">
                            <p>Basic Info</p>
                        </div>
                        <div class="d_customer_row_info">
                            <p class="span_p"><span class="d_span_1">Name</span><span
                                    class="d_span_2">:</span> <span
                                    class="d_span_3">{{auth()->user()->name}}</span></p>
                            <p class="span_p"><span class="d_span_1">Email</span><span
                                    class="d_span_2">:</span> <span
                                    class="d_span_3">{{auth()->user()->email}}</span></p>
                            <p class="span_p"><span class="d_span_1">Phone Number</span><span
                                    class="d_span_2">:</span> <span class="d_span_3"></span>
                            </p>
                        </div>
                        <div class="d_customer_row_title mt-4">
                            <p>Security & Address</p>
                        </div>
                        <div class="d_customer_row_address">
                            <p class="span_p"><span class="d_span_1">Password</span><span
                                    class="d_span_2">:</span> <span class="d_span_3">********</span></p>
                            <p class="shipping_address">Shipping Address : </p>
                            <p> <span class="a_span_1"> Address </span> <span class="a_span_2">:</span>
                                <span class="a_span_3">House Number 00, Road 12/5 ,Sector A</span></p>
                            <div class="multiple_address">
                                <p> <span class="a_span_1"> City </span> <span class="a_span_2">:</span>
                                    <span class="a_span_3">Dhaka</span></p>
                                <p> <span class="a_span_1"> Country </span> <span class="a_span_2">:</span>
                                    <span class="a_span_3">Bangladesh</span></p>
                                <p> <span class="a_span_1"> Post Code </span> <span
                                        class="a_span_2">:</span> <span class="a_span_3">2200</span></p>

                            </div>
                            <p class="shipping_address">Billing Address : </p>
                            <p> <span class="a_span_1"> Address </span> <span class="a_span_2">:</span>
                                <span class="a_span_3">House Number 00, Road 12/5 ,Sector A</span></p>
                            <div class="multiple_address">
                                <p> <span class="a_span_1"> City </span> <span class="a_span_2">:</span>
                                    <span class="a_span_3">Dhaka</span></p>
                                <p> <span class="a_span_1"> Country </span> <span class="a_span_2">:</span>
                                    <span class="a_span_3">Bangladesh</span></p>
                                <p> <span class="a_span_1"> Post Code </span> <span
                                        class="a_span_2">:</span> <span class="a_span_3">2200</span></p>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div role="tabpanel" class="tab-pane  fade" id="hotels">

            </div>
            <div role="tabpanel" class="tab-pane  fade" id="reviews">

            </div>
        </div>
    </div>
    @endsection
