<div class="customer_d_tab_left">
    <ul class="nav nav-tabs flex-column " role="tablist">
        <li class="nav-item">
            <a class="nav-link" href="#d_customer_info" role="tab" data-toggle="tab">Customer
                Information</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#hotels" role="tab" data-toggle="tab">Hotels</a>
        </li>
        @if(auth()->user()->user_type == 4)
        <li class="nav-item">
            <a class="nav-link @if($globalData['routeName'] == 'frontend.become.vendor') active @endif" href="{{route('frontend.become.vendor')}}">Become A Vendor</a>
        </li>
        @endif
    </ul>
</div>
