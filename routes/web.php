<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

include('parvez.php');
include('frontend.php');

Route::get('/admin-login','AdminLoginController@loginView')->name('admin.login');
Route::post('/admin-login-submit','AdminLoginController@loginSubmit')->name('admin.login.submit');

Route::group(['middleware'=>'authCheck'],function (){
	
	 Route::get("/dashboard","DashboardController@index");

    Route::resource("/activity","ActivityController");

    Route::resource("/module","ModuleController");

    Route::resource("/role","RoleController");

    Route::resource("/user","UserController");

    Route::get('role-access',"RoleAccessController@index")->name('role.access');
    Route::post('roleAclSetup', 'RoleAccessController@roleAclSetup');
    Route::post('roleacl', 'RoleAccessController@save');

    Route::get('user-access', 'UserAccessController@index')->name('user.access');
    Route::post('userAclSetup', 'UserAccessController@userAclSetup');
    Route::post('useracl', 'UserAccessController@save');

    Route::get('/backend',function(){
        return view('backend.demo');
    });

    Route::get('/logout','AdminLoginController@logout')->name('admin.logout');

    Route::get('/dashboard',function(){
        return view('backend.demo');
    })->name('backend.dashboard');
    
    Route::get('/form',function(){
        return view('backend.form');
    });

    Route::resource("/discount", "DiscountController");
    Route::post('get-discount-user-list', 'DiscountController@getDiscountUserList')->name('discount-user.list');
    Route::resource("/shipping-option", "ShippingOptionController");
    Route::resource("/shop-setting", "ShopSettingController");
    Route::get("/show-logo-banner/{action}/{id}", "ShopSettingController@showLogoBanner")->name("show-logo-banner.show");
    Route::resource("/vendor-page", "VendorPageController");
    Route::resource("/translation-language", "TranslationLanguageController");
    Route::resource("/countries", "CountriesController");
    Route::resource("/state", "StateController");
    Route::resource("/city", "CityController");

    Route::resource("store-review","StoreReviewController");
    Route::get('orders','OrderController@index')->name('backend.order.view');
    Route::get('orders-details/{id}/{action?}','OrderController@orderDetails')->name('backend.order.details');
    Route::post('orders-status-change','OrderController@orderStatusChange')->name('backend.orders-status-change');

    Route::resource('payment-gateway','PaymentGatewayController');
    Route::resource('currencys','CurrencyController');
    Route::resource('email-configuration','EmailConfigurationController');
    Route::resource('products','ProductController');

    Route::get('products-gallery/{slug}','ProductGalleryController@galleryImage')->name('product.gallery');
    Route::post('products-gallery/product.gallery.store','ProductGalleryController@galleryImageStore')->name('product.gallery.store');
    Route::get('products-gallery/delete/{id}','ProductGalleryController@galleryImageDelete')->name('product.gallery.delete');

    //product route start
    Route::get('partho-product/create','ParthoProductController@ProductCreate')->name('product_create');
    Route::get('partho-product/list','ParthoProductController@ProductList')->name('product_list');
    Route::get('partho-product/details/{id}','ParthoProductController@productDetails')->name('product_details');
    Route::get('partho-product/edit/{id}','ParthoProductController@productEdit')->name('product_edit');
    Route::post('partho-product/store/','ParthoProductController@productStore')->name('product_store');
    Route::post('partho-product/update/','ParthoProductController@productUpdate')->name('product_update');
    Route::get('partho-import/product','ParthoProductController@importProduct')->name('import_product');
    //product route end

    Route::resource('vendor-subscription-plan','VendorSubscriptionPlansController');

//Ajax Route
    Route::post('get/subcategory','AjaxController@subCategory')->name('get.subcategory');
include('farhan.php');
});

