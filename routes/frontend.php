<?php
Route::get("/", "HomeController@index")->name('home.index');

Route::get('/page/{slug}', 'HomeController@customPage')->name('home.custom.page');

Route::get('/user-login', 'UserLoginController@loginView')->name('user.login');
Route::post('/user-login-submit', 'UserLoginController@loginSubmit')->name('user.login.submit');

Route::post('/user-registration-submit', 'UserController@store')->name('user.registration.submit');


Route::get('user/login/facebook', 'UserLoginController@redirectToProvider')->name('user.facebook.login');
Route::get('user/login/facebook/callback', 'UserLoginController@handleProviderCallback');

Route::get('user/login/google', 'UserLoginController@redirectToProviderGoogle')->name('user.google.login');
Route::get('user/login/google/callback', 'UserLoginController@handleProviderGoogleCallback');

Route::get('/product-detail/{slug}', 'frontend\ProductDetailController@index')->name('product.detail');

Route::group(['middleware' => 'authCeckUser'], function () {
    Route::resource("/user", "UserController");
    Route::get('/user-logout', 'UserLoginController@logout')->name('user.logout');

    Route::get('/become-vendor', 'frontend\UserDashboardController@BecomeVendor')->name('frontend.become.vendor');
    Route::post('/vendor/subscription', 'frontend\UserDashboardController@vendorSubscription')->name('frontend.vendor.subscription');
});
