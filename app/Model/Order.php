<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $keyType = 'string';

    public function orderDetail() {
        return $this->hasMany(OrderDetail::class,'order_id','id');
    }

    public function orderAddress() {
        return $this->hasOne(OrderShippingAddress::class,'order_id','id');
    }

    public function shipping_method_name() {
        return $this->belongsTo(ShippingOption::class, 'shipping_method');
    }

    public function payment_gateway() {
        return $this->belongsTo(PaymentGateway::class, 'payment_method');
    }
}
