<?php

namespace App\Http\Controllers;

use App\CustomClass\OwnLibrary;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\User;
use App\Model\OrderShippingAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use DB;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    private $moduleId = 1;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $orders = Order::whereNotNull('id')->paginate(10);

        $orders = Order::join('order_details', 'order_details.order_id', '=', 'orders.id')
                        ->join('order_shipping_addresses', 'order_shipping_addresses.order_id', '=', 'orders.id')
                        ->join('users', 'users.id', '=', 'order_details.vendor_id')
                        ->select("orders.*", "orders.id as oid", "users.name as vendor_name"
                            , DB::raw("SUM(order_details.quantity) as quantity")
                            , DB::raw("SUM(order_details.total) as order_total")
                        );
                        if(!empty($request->vendor_id)){
                            $orders->where('order_details.vendor_id','=',$request->vendor_id);
                        }
                        if(!empty($request->order_no)){
                            $orders->where('orders.order_number','=',$request->order_no);
                        }
                        if(!empty($request->order_status)){
                            $orders->where('orders.order_status','=',$request->order_status);
                        }
                        if(!empty($request->start_date)){
                            $orders->whereDate('orders.created_at', '>=', $request->start_date);
                        }
                        if(!empty($request->end_date)){
                            $orders->whereDate('orders.created_at', '<=', $request->end_date);
                        }
                        if(Auth::user()->role_id == 10){
                            $orders->where('order_details.vendor_id','=',Auth::id());
                        }
                        $orders = $orders->groupBy('orders.id')
                        ->paginate(10);

        $users = User::where("role_id","=",10)->get();
        return view('backend.order.index',compact('orders','request','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function orderDetails($id, $action=null){

        $orders = Order::find($id);
        $order_details = OrderDetail::where("order_id","=",$id);
                        if(Auth::user()->role_id == 10){
                            $order_details->where('order_details.vendor_id','=',Auth::id());
                        }
        $order_details = $order_details->get();

        $shipping = OrderShippingAddress::where("order_id","=",$id)->first();

        if($action!='print'){
            return view('backend.order.order-detail',compact('orders', 'order_details', 'shipping'));
        }else{
            return view('backend.order.print-order-details',compact('orders', 'order_details', 'shipping'));
        }
    }

    public function orderStatusChange(Request $request){
        if($request->has('order_id')){

            if(Auth::user()->role_id == 10){
                $order_details = OrderDetail::where('order_id', $request->order_id)->where("vendor_id","=",Auth::id())->update(['order_status'=>$request->order_value]);
            }else{
                $order_details = OrderDetail::where('order_id', $request->order_id)->update(['order_status'=>$request->order_value]);

                $orders = Order::find($request->order_id);
                $orders->order_status = $request->order_value;
                $orders->save();
            }

            return 'success';
        }else{
            return 'faild';
        }
    }

}
