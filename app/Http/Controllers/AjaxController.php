<?php

namespace App\Http\Controllers;

use App\Model\Category;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function subCategory(Request $request){

        $subcategories = Category::select('id','category_name')->orderBy('category_name')->where('status','=',1)->where('parent_id','=',$request->category_id)->get();
        return response()->json($subcategories);
    }
}
