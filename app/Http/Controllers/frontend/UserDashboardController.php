<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\VendorSubscription;
use App\Model\VendorSubscriptionPlans;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class UserDashboardController extends Controller
{
    public function BecomeVendor(){
        $vendorPlans = VendorSubscriptionPlans::all();
        return view('frontend.dashboard.become-vendor',compact('vendorPlans'));
    }

    public function vendorSubscription(Request $request){
        $plan = new VendorSubscription();
        $plan->user_id = auth()->id();
        $plan->vendor_subscription_plan_id = $request->plan;
        $plan->expaire_date = Carbon::now()->addDays(30);
        if ($plan->save()){
            $user = User::find(auth()->id());
            $user->user_type = 2;
            $user->save();
            Session::flash('success','You have successfully become a vendor');
            return Redirect::route('backend.dashboard');
        }else{
            Session::flash('error','Unable to subscribe, Please try again');
            return redirect()->back();
        }
    }
}
