<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Product;


class ProductDetailController extends Controller
{
    public function index($slug)
    {

        $productDetail     = Product::with('category', 'vendor', 'brand', 'user', 'gallery')->where('slug', $slug)->first();
        $moreProductShop   = Product::where('vendor_id', $productDetail->vendor_id)->where('id', '!=', $productDetail->id)->limit(3)->get();
        $reletedProduct    = Product::with('category', 'vendor', 'brand', 'user')->where('id', '!=', $productDetail->id)->orWhere('category_id', $productDetail->category_id)->orWhere('subcategory_id', $productDetail->subcategory_id)->get();



        return view('frontend.product.product-details', compact('productDetail', 'moreProductShop', 'reletedProduct'));
    }
}
